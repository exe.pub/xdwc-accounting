#!/bin/sh

set -e

if [ "$1" == "--build" ]
then
    docker build -t xdwc-accounting .
fi

docker run -v "$(pwd)/xdwc.data:/xdwc.data:ro" -p 5000:5000 xdwc-accounting
