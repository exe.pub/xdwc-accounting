FROM archlinux:latest

RUN pacman -Syu --noconfirm && pacman --noconfirm -S hledger hledger-web

CMD ["hledger-web", "--server", "--capabilities=view", "--host=0.0.0.0", "-f", "/xdwc.data"]
